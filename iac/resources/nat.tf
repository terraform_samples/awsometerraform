module "nat" {
  source     = "GoogleCloudPlatform/nat-gateway/google"
  region     = "${var.region}"
  network    = "default"
  subnetwork = "default"
}
